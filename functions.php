<?php

function my_theme_enqueue_styles() {
    $min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
    $parent_style = 'covernews-style';

    $fonts_url = 'https://fonts.googleapis.com/css?family=Cabin:400,400italic,500,600,700';
    wp_enqueue_style('covermag-google-fonts', $fonts_url, array(), null);
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap' . $min . '.css');    
    wp_enqueue_style('events-manager-child', get_template_directory_uri() . '-child/events_manager.css' );
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
    
    wp_enqueue_style(
        'covermag',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'bootstrap', $parent_style ),
        wp_get_theme()->get('Version') );


}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );



// Deregistramos los estilos del plugin del evento
function deregister_my_styles() {
    // wp_deregister_style( 'events-manager-css' );
    wp_dequeue_style( "events-manager" ); 
}
add_action( 'wp_print_styles', 'deregister_my_styles', 100 );